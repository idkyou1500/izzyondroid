package in.sunilpaulmathew.izzyondroid.utils;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on September 04, 2021
 */
public class CategoryMenuItems implements Serializable {

    private final Drawable mIcon;
    private final int mColor;
    private final String mTitle;

    public CategoryMenuItems(Drawable icon, int color, String title) {
        this.mIcon = icon;
        this.mColor = color;
        this.mTitle = title;
    }

    public Drawable getIcon() {
        return mIcon;
    }

    public int getColor() {
        return mColor;
    }

    public String getTitle() {
        return mTitle;
    }

}